let port = require("http");

port.createServer
( 
	function(request, response)
	{

		if(request.url == '/index'){
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Welcome to the login page')
		}
		else {
	        response.writeHead(404, {'Content-Type': 'text/plain'})
	        response.end('Page not available')
	    }
	}
).listen(3000);//assigning port

console.log('Server is running at localhost:3000');